const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const handlebars = require('express-handlebars');
const app = express();
const urlEncodeParser = bodyParser.urlencoded({ extended: false });

const sql = mysql.createConnection({
    host: 'localhost',
    user: 'hygino',
    password: '89631139',
    port: 3306
});

sql.query('USE nodejs_crud');
app.use('/img', express.static('img'));
app.use('/css', express.static('css'));
app.use('/js', express.static('js'));

//template engine
app.engine("handlebars", handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

//routes and templates
app.get("/", function (req, res) {
    res.render('index'/*, { id: req.params.id }*/);
});

app.get('/javascript', function (req, res) {
    res.sendFile(__dirname + '/js/javascript.js');
});

app.get('/style', function (req, res) {
    res.sendFile(__dirname + '/css/style.css');
});

app.get('/inserir', function (req, res) {
    res.render('inserir');
});

app.get("/select/:id?", function (req, res) {
    if (!req.params.id) {
        sql.query("SELECT * FROM tb_user ORDER BY id ASC", function (err, results, fields) {
            res.render('select', { data: results });
        });
    } else {
        sql.query("SELECT * FROM tb_user WHERE id=? ORDER BY id ASC", [req.params.id], function (err, results, fields) {
            res.render('select', { data: results });
        });
    }
});

app.post('/controllerForm', urlEncodeParser, function (req, res) {
    sql.query('INSERT INTO tb_user(name,age) VALUES(?, ?)', [req.body.name, req.body.age]);
    res.render('controllerForm', { name: req.body.name });
});

app.get('/deletar/:id', function (req, res) {
    sql.query("DELETE FROM tb_user WHERE id=?", [req.params.id]);
    res.render('deletar');
});

app.get("/update/:id", function (req, res) {
    sql.query("SELECT * FROM tb_user WHERE id=?", [req.params.id], function (err, results, fields) {
        res.render('update', { id: req.params.id, name: results[0].name, age: results[0].age });
    });
});

app.post("/controllerUpdate", urlEncodeParser, function (req, res) {
    sql.query("UPDATE tb_user SET name=?,age=? WHERE id=?", [req.body.name, req.body.age, req.body.id]);
    res.render('controllerUpdate');
});

//start server
app.listen(3000, function (req, res) {
    console.log('O servidor está rodando!');
});